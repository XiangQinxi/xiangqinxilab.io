# 首页
😆哈喽，这里是我的文档网站

## 我的项目

=== "ttkthemes2"

    [PyPI](https://pypi.org/project/ttkthemes2/)
    
    受`ttkthemes`灵感而发，补充一些缺少的主题


=== "tkfly"

    - 主要库
    [PyPI](https://pypi.org/project/tkfly/)

    - 基础库
    [PyPI](https://pypi.org/project/tkmfly/)

    集合`tcl/tk`、`winforms`等各种扩展库制作的大型组件库、功能库

=== "tkscrollutil"

    [PyPI](https://pypi.org/project/tkscrollutil/)
    
    基于`tcl/tk`的扩展库`tkscrollutil`的组件库，关于滚动条、滚动容器类的扩展

=== "tkadw"

    [PyPI](https://pypi.org/project/tkadw/)

    使用`tkinter.Canvas`绘制出来的组件库

=== `tkwinico`

    [PyPI](https://pypi.org/project/tkwinico/)

    基于`tcl/tk`的扩展库`winico`的组件库，关于Windows平台下的系统托盘功能